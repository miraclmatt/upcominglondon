---
title: Upcoming London | Find out what's on in the capital
menu: Home

body_classes: "modular header-image fullwidth"

content:
    items: @self.modular
    order:
        by: default
        dir: asc
        custom:
            - _showcase
            - _highlights
---
