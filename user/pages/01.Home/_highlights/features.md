---
title: Homepage Highlights
menu: Listings
class: center
---
| Pubs                                                                               | Gig venues                                                                       | Clubs                                                               |
|------------------------------------------------------------------------------------|----------------------------------------------------------------------------------|---------------------------------------------------------------------|
| [Dogstar](http://dogstarbrixton.com)                                               | [100 Club](http://www.the100club.co.uk/events-calendar)                          | [333](http://www.333oldstreet.com)                                  |
| [The Dublin Castle](http://thedublincastle.com)                                    | [Brixton Academy](https://www.academymusicgroup.com/o2academybrixton/events/all) | [Basing House](http://basinghouse.co.uk/listings)                   |
| [Fiddlers Elbow](http://www.thefiddlerselbow.co.uk/whos-playing-at-fiddlers-elbow) | [Cafe Oto](https://www.cafeoto.co.uk/events)                                     | [Bloc](https://www.facebook.com/pg/blocwknd/events)          |
| [The Finsbury](http://www.thefinsbury.co.uk/listings)                              | [Camden Assembly](http://camdenassembly.com/whats-on)                            | [The Book Club](http://www.wearetbc.com/whats-on)                   |
| [The Horse and Groom](http://thehorseandgroom.net)                                 | [Camden Underworld](https://www.theunderworldcamden.co.uk/gigs)                  | [Club 414](http://club414.org/calendar)                             |
| [The Lexington](http://thelexington.co.uk/calendar)                                | [Dingwalls](http://dingwalls.com/listings)                                       | [Club Aquarium](http://www.clubaquarium.co.uk/events)               |
| [The Old Blue Last](http://www.theoldbluelast.com/listings)                        | [Electric Ballroom](http://electricballroom.co.uk/whats-on)                      | [Corsica Studios](http://www.corsicastudios.com/whats-on)           |
| [The Old Queens Head](http://theoldqueenshead.com/whats-on)                        | [Electrowerkz](https://www.facebook.com/pg/Electrowerkz-497558173609057/events)  | [Egg London](http://www.egglondon.co.uk/events)                     |
| [Shacklewell Arms](https://www.shacklewellarms.com/listings)                       | [The Garage](http://thegarage.london/listings)                                   | [Electric Brixton](http://electricbrixton.uk.com/events.php)        |
|                                                                                    | [Hammersmith Apollo](https://www.eventimapollo.com/events)                       | [Fabric](https://www.fabriclondon.com/club/listings)                |
|                                                                                    | [Islington Academy](https://academymusicgroup.com/o2academyislington/events/all) | [Koko](http://www.koko.uk.com/listings)                             |
|                                                                                    | [Jazz Cafe](http://thejazzcafelondon.com/whats-on)                               | [Ministry of Sound](http://club.ministryofsound.com/listings)       |
|                                                                                    | [Nambucca](http://www.nambucca.co.uk/events)                                     | [The Nest](http://www.ilovethenest.com/whats-on)                    |
|                                                                                    | [Oval Space](https://ovalspace.co.uk/events)                                     | [Slimelight](https://www.facebook.com/pg/SlimelightOfficial/events) |
|                                                                                    | [Proud Camden](http://proudcamden.com/listings-tickets)                          | [Village Underground](http://www.villageunderground.co.uk/events)   |
|                                                                                    | [Rye Wax (aka Bussey Building)](https://www.facebook.com/pg/ryewax/events)        | [XoYo](http://xoyo.co.uk/whats-on/?range=weekends)                  |
|                                                                                    | [Scala](http://scala.co.uk/all-events-list)                                      |                                                                     |
|                                                                                    | [The Water Rats](http://thewaterratsvenue.london/listings.html)                  |                                                                     |
